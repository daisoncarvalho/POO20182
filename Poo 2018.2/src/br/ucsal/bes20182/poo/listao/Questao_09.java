package br.ucsal.bes20182.poo.listao;

// Convers�o de Celsius para Fahrenheit

import java.util.Scanner;

public class Questao_09 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);		
		double cel, fah;
		System.out.println("Informe um valor para ser convertido de Celsius para Fahrenheit: ");
		cel = sc.nextDouble();
		fah = (9 * cel + 160) / 5;
		System.out.println("A temperatura convertida de Celsius para Fahrenheit �: " + fah);
		sc.close();
	}

}
