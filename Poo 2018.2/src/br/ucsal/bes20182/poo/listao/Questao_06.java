package br.ucsal.bes20182.poo.listao;

// leitura de nome e sobrenome

import java.util.Scanner;

public class Questao_06 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String nome, sobrenome;
		System.out.println("Informe o seu primeiro nome:");
		nome = sc.next();
		System.out.println("Informe o seu sobrenome: ");
		sobrenome = sc.next();
		System.out.println("Nome completo: " + nome + " " + sobrenome);
		sc.close();
	}

}
