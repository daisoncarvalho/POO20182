package br.ucsal.bes20182.poo.listao;

// Volume de uma lata de �leo

import java.util.Scanner;

public class Questao_11 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double volume, raio, altura, pi= 3.14159;
		System.out.println("Informe o valor do raio: ");
		raio = sc.nextDouble();
		System.out.println("Informe o valor da altura: ");
		altura = sc.nextDouble();
		volume = pi * (raio * raio) * altura; 
		System.out.println("O valor do volume �: " + volume);
		sc.close();
		
	}

}
