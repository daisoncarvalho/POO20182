package br.ucsal.bes20182.poo.listao;

import java.util.Scanner;

// C�lculo de trabalho

public class Questao_08 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		double VH, PD, SB, TD, SL;
		int HT;
		System.out.println("Informe o n�mero de horas trabalhadas no mes: ");
		HT = sc.nextInt();
		System.out.println("Informe o valor da hora trabalhada: ");
		VH = sc.nextDouble();
		System.out.println("Informe o percentual de desconto: ");
		PD = sc.nextInt();

		SB = (HT * VH); 
		TD = (PD / 100) * SB;
		SL = SB - TD;
		System.out.println("Horas trabalhadas: " + HT + "\nSal�rio bruto: " + SB + "\nTotal de Desconto: " + TD + "\nSal�rio L�quido: " + SL);
		sc.close();
	}

}
