package br.ucsal.bes20182.poo.listao;

import java.util.Scanner;

// Calculando a �rea de um tri�ngulo

public class Questao_05 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Double area, base, altura;
		System.out.println("////////// CALCULANDO A �REA DE UM TRI�NGULO //////////" + "\n�REA = (BASE * ALTURA) / 2");
		System.out.println("\nInforme o valor da base do tri�ngulo: ");
		base = sc.nextDouble();
		System.out.println("Informe o valor da altura do triangulo: ");
		altura = sc.nextDouble();
		area = (base * altura) / 2;
		System.out.println("Valor da base: " + base + "\nValor da altura: " + altura + "\nO valor da �rea do tri�ngulo �: " + area);
		sc.close();
	}

}
