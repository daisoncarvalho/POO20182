package br.ucsal.bes20182.poo.listao;

// Soma de valores

import java.util.Scanner;

public class Questao_07 {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		int valor1, valor2, soma;
		System.out.println("Informe o primeiro valor: ");
		valor1 = sc.nextInt();
		System.out.println("Informe o segundo valor: ");
		valor2 = sc.nextInt();
		soma = valor1 + valor2;
		System.out.println("\nValor 1 escolhido: " + valor1 + "\nValor 2 escolhido: " + valor2 + "\nA soma dos dois valores �: " + soma);
		sc.close();
	}

}
