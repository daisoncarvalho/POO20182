package br.ucsal.poo.locadora;

import java.util.ArrayList;

import java.util.List;

public class Contrato {
	private static int cont = 0;
	private String nome;
	private String endere�o;
	private static Double valorTotal=0.;
	List<Veiculo> veiculos = new ArrayList<Veiculo>();

	public Contrato(String nome, String endere�o) {
		this.nome = nome;
		this.endere�o = endere�o;
		cont++;

	}

	public Double Somar() {
		for (int i = 0; i < veiculos.size(); i++) {
			valorTotal = veiculos.get(i).getValor();
		}
		return valorTotal;
	}

	public static int getCont() {
		return cont;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndere�o() {
		return endere�o;
	}

	public void setEndere�o(String endere�o) {
		this.endere�o = endere�o;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public Boolean adicionarVeiculo(Veiculo veiculo) {
		if (!veiculos.contains(veiculo)) {
			veiculos.add(veiculo);
			return true;
		} else {
			System.out.println(
					"N�o foi possivel adicionar o carro: " + veiculo.getPlaca() + " pois j� existe um no sistema");
			return false;
		}
	}

	public Boolean removerVeiculo(String placa) {
		for (int i = 0; i < veiculos.size(); i++) {
			if (veiculos.get(i).getPlaca().equals(placa)) {
				veiculos.remove(i);
				return true;
			}
		}
		return false;
	}

	public List<Veiculo> consultarVeiculos(String tipoVeiculo) {
		for (int i = 0; i < veiculos.size(); i++) {
			if (veiculos.get(i).getTipo().equals(tipoVeiculo)) {
				return veiculos;
			}
		}
		return null;
	}

	public List<Veiculo> consultarVeiculos() {
		return veiculos;
	}

	public Veiculo consultarVeiculo(String placa) {
		for (int i = 0; i < veiculos.size(); i++) {
			if (veiculos.get(i).getPlaca().equals(placa)) {

				return veiculos.get(i);
			}
		}
		return null;
	}

}
