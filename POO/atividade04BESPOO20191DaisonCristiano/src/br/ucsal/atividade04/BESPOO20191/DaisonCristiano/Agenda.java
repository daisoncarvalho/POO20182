package br.ucsal.atividade04.BESPOO20191.DaisonCristiano;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
	private static List<Contato> agenda = new ArrayList<>();

	public static void adicionar(Contato contato) {
		agenda.add(contato);

	}

	public void remover(Contato contato) {

		agenda.remove(contato);

	}

	public List<Contato> listarContatos() {

		return agenda;

	}

	public void PesquisarPorNome(String nome) {
		Integer n = 0;
		
		for (Contato atual : agenda) {
			if (atual.getNome().contains(nome)) {
				System.out.println(atual);
			}else {
				n++; 
			}
			if (n==agenda.size()) {
				System.out.println("Contato n�o est� na lista!!");
			}
			

		}
	}
}
