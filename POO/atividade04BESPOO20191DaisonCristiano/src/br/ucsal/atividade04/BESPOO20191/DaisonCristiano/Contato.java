package br.ucsal.atividade04.BESPOO20191.DaisonCristiano;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Contato {
	private String nome;
	private String telefone;
	private Date dataNascimento;
	private TipoENUM tipo;
	private String formattdate;

	public Contato(String nome, String telefone, String conveter, String teste) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.tipo = TipoENUM.valueOf(teste.toUpperCase());

		converterData(conveter);

	}

	public TipoENUM getTipo() {
		return tipo;
	}

	public void setTipo(TipoENUM tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public void converterData(String converter) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			dataNascimento = df.parse(converter);
			formattdate = df.format(dataNascimento);
		} catch (Exception e) {

		}

	}

	@Override
	public String toString() {
		return "Contato [nome= " + nome + ", telefone= " + telefone + ", data Nascimento= " + formattdate + ", tipo= "
				+ tipo + "]" ;
	}

}
